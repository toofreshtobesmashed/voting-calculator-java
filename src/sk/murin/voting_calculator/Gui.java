package sk.murin.voting_calculator;

import javax.swing.*;
import java.awt.*;

public class Gui {
    private int fullVotes = 0;
    private int numOfVotes;
    private int votesFirst = 0;
    private int votesSecond = 0;
    private int votesThird = 0;
    private int votesLeft;
    private JLabel labelMessage;
    private final JLabel[] labelInfo = new JLabel[3];
    private final Font f = new Font("Calibri", Font.BOLD, 20);
    private final Font fontLabels = new Font("Calibri", Font.BOLD, 12);
    private final JButton[] buttonsForPressing = new JButton[3];
    private final JButton[] buttonsForShowing = new JButton[3];

    public Gui() {
        setup();
    }

    private void setup() {
        JFrame frame = new JFrame("Elections");
        frame.setSize(700, 450);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.add(makePanelTop(), BorderLayout.NORTH);
        frame.add(makePanelCenter(), BorderLayout.CENTER);
        frame.add(makePanelBottom(), BorderLayout.SOUTH);
        frame.setResizable(false);
        frame.setVisible(true);
        howManyVotes();
    }

    //panel top
    private JPanel makePanelTop() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 1));
        labelMessage = new JLabel("You have " + numOfVotes + " votes");
        labelMessage.setHorizontalAlignment(SwingConstants.CENTER);
        labelMessage.setFont(f);
        panel.add(labelMessage);
        return panel;
    }

    //_______________________________________________________________
    //panel center
    private JPanel makePanelCenter() {
        JPanel panel = new JPanel();
        panel.setLayout(null);

        for (int i = 0; i < buttonsForShowing.length; i++) {
            buttonsForShowing[i] = new JButton();
            buttonsForShowing[i].setEnabled(false);
            panel.add(buttonsForShowing[i]);
        }

        buttonsForShowing[0].setBounds(70, 265, 140, 1);
        buttonsForShowing[1].setBounds(280, 265, 140, 1);
        buttonsForShowing[2].setBounds(493, 265, 140, 1);

        for (int i = 0; i < labelInfo.length; i++) {
            labelInfo[i] = new JLabel();
            labelInfo[i].setFont(fontLabels);
            panel.add(labelInfo[i]);
        }

        labelInfo[0].setText("You have 0 votes, its % from " + fullVotes);
        labelInfo[0].setBounds(45, 300, 210, 20);
        labelInfo[1].setText("You have 0 votes, its % from " + fullVotes);
        labelInfo[1].setBounds(265, 300, 210, 20);
        labelInfo[2].setText("You have 0 votes, its % from " + fullVotes);
        labelInfo[2].setBounds(475, 300, 210, 20);
        return panel;
    }

    //_______________________________________________________________
    //panel Bottom
    private JPanel makePanelBottom() {

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());

        for (int i = 0; i < buttonsForPressing.length; i++) {
            buttonsForPressing[i] = new JButton();
            buttonsForPressing[i].setFont(f);
            buttonsForPressing[i].setFocusable(false);
            panel.add(buttonsForPressing[i]);
        }

        buttonsForPressing[0].setText("add to 1.group");
        buttonsForPressing[0].addActionListener(e -> addToFirstGroup());
        buttonsForPressing[0].setBounds(70, 330, 140, 50);
        buttonsForPressing[1].setText("add to 2.group");
        buttonsForPressing[1].addActionListener(e -> addToSecondGroup());
        buttonsForPressing[1].setBounds(280, 330, 140, 50);
        buttonsForPressing[2].setText("add to 3.group");
        buttonsForPressing[2].addActionListener(e -> addToThirdGroup());
        buttonsForPressing[2].setBounds(493, 330, 140, 50);
        return panel;

    }

    private void addToFirstGroup() {
        buttonsForShowing[0].setSize(buttonsForShowing[0].getWidth(), buttonsForShowing[0].getHeight() + 2);
        buttonsForShowing[0].setLocation(buttonsForShowing[0].getX(), buttonsForShowing[0].getY() - 2);
        fullVotes++;
        votesFirst++;
        votesLeft--;

        labelSetText();
        calculate();
        if (fullVotes == numOfVotes) {
            for (JButton jButton : buttonsForPressing) {
                jButton.setEnabled(false);
            }
            changeColor();
        }
    }

    private void addToSecondGroup() {
        buttonsForShowing[1].setSize(buttonsForShowing[1].getWidth(), buttonsForShowing[1].getHeight() + 2);
        buttonsForShowing[1].setLocation(buttonsForShowing[1].getX(), buttonsForShowing[1].getY() - 2);
        fullVotes++;
        votesSecond++;
        votesLeft--;

        labelSetText();
        calculate();
        if (fullVotes == numOfVotes) {
            for (JButton jButton : buttonsForPressing) {
                jButton.setEnabled(false);
            }
            changeColor();
        }

    }

    private void addToThirdGroup() {
        buttonsForShowing[2].setSize(buttonsForShowing[2].getWidth(), buttonsForShowing[2].getHeight() + 2);
        buttonsForShowing[2].setLocation(buttonsForShowing[2].getX(), buttonsForShowing[2].getY() - 2);
        fullVotes++;
        votesThird++;
        votesLeft--;

        labelSetText();
        calculate();
        if (fullVotes == numOfVotes) {
            for (JButton jButton : buttonsForPressing) {
                jButton.setEnabled(false);
            }
            changeColor();
        }
    }

    private void labelSetText() {
        labelMessage.setText("You have " + numOfVotes + " votes, " + votesLeft + " votes left");
    }

    private void howManyVotes() {
        while (true) {
            try {
                int votes = Integer.parseInt(JOptionPane.showInputDialog(null, "How many votes do you have?", "?", JOptionPane.QUESTION_MESSAGE));
                if (0 < votes && votes <= 100) {
                    numOfVotes = votesLeft = votes;
                    labelSetText();
                    break;
                } else {
                    JOptionPane.showMessageDialog(null, "Enter value between 1 - 100", "Enter correct values", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Enter value between 1 - 100", "Enter correct values", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        for (JLabel jLabel : labelInfo) {
            jLabel.setText("You have 0 votes, its 0% from " + numOfVotes);
        }

    }

    private void calculate() {
        double percentageFirst = (double) votesFirst / fullVotes;
        double percentageSecond = (double) votesSecond / fullVotes;
        double percentageThird = (double) votesThird / fullVotes;

        labelInfo[0].setText(String.format("You have %d votes, its %.1f %% from %d", votesFirst, percentageFirst * 100, numOfVotes));
        labelInfo[1].setText(String.format("You have %d votes, its %.1f %% from %d", votesSecond, percentageSecond * 100, numOfVotes));
        labelInfo[2].setText(String.format("You have %d votes, its %.1f %% from %d", votesThird, percentageThird * 100, numOfVotes));

    }

    private void changeColor() {
        if (votesFirst > votesSecond && votesFirst > votesThird) {
            buttonsForShowing[0].setBackground(Color.GREEN);
            JOptionPane.showMessageDialog(null, "First won", "End", JOptionPane.INFORMATION_MESSAGE);
        } else if (votesSecond > votesThird && votesSecond > votesFirst) {
            buttonsForShowing[1].setBackground(Color.GREEN);
            JOptionPane.showMessageDialog(null, "Second won", "End", JOptionPane.INFORMATION_MESSAGE);
        } else if (votesThird > votesFirst && votesThird > votesSecond) {
            buttonsForShowing[2].setBackground(Color.GREEN);
            JOptionPane.showMessageDialog(null, "Third won", "End", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Nobody won", "End", JOptionPane.INFORMATION_MESSAGE);
        }
    }


}
